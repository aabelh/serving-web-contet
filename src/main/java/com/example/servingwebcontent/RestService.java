package com.example.servingwebcontent;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;

@Service
public class RestService {

    private final RestTemplate restTemplate;

    public RestService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }
    private String baseUrl = "http://localhost:9091";
    public String getName() {
        String endpoint = "/getName";
        return this.restTemplate.getForObject(baseUrl + endpoint, String.class);
    }

    public void sendDataToRestService() {
        String endpoint = "/feedback";
        restTemplate.postForObject(baseUrl+endpoint, "Am trimis feedback la ora::" + LocalDateTime.now(), String.class);
    }
}
